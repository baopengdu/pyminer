# PyMiner

#### 描述
PyMiner一款数据分析工具,目的是使pandas\sklearn的操作进行可视化，项目许可遵循MIT 开源协议。

#### 技术说明
项目开发环境基于Window 10 X64，使用Python3.8+PyQt5.15+Pycharm进行技术开发。

#### 安装

1. 下载项目源码 
2. 安装python并打开命令行工具，使用 pip install -r requirements.txt 导入python包，如果你的python依赖包下载太慢，建议使用：pip install -i https://mirrors.cloud.tencent.com/pypi/simple -r requirements.txt
3. 调用python 执行目录下pyminer.py，例如python安装在C盘根目录下，可以在cmd命令行中执行：C:\python\python.exe C:\PyMiner\pyminer.py
4. 此外，你也可以使用pyinstaller进行编译后使用，编译语句：pyinstaller -i logo.ico -w pyminer.py


#### 联系我

1.  作者：PyMiner Development Team
2.  邮箱：aboutlong@qq.com
3.  Q Q：454017698
4.  QQ群：945391275

#### 预览

预览1
![avatar](https://s1.ax1x.com/2020/08/21/dY7WtA.md.png)

预览2
![avatar](https://s1.ax1x.com/2020/08/21/dY7I6f.md.png)

预览3
![avatar](https://s1.ax1x.com/2020/08/21/dY7oX8.md.png)

预览4
![avatar](https://s1.ax1x.com/2020/08/21/dY751P.md.png)

预览5
![avatar](https://s1.ax1x.com/2020/08/21/dY74pt.md.png)